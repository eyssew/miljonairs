<?php
class checkAnswer {
	private $correct_answer = [
		1 => 'D',
		2 => 'B',
		3 => 'B',
		4 => 'D',
		5 => 'C',
		6 => 'B',
		7 => 'C',
		8 => 'A',
		9 => 'C',
		10 => 'B',
		11 => 'A',
		12 => 'C',
		13 => 'B',
		14 => 'A',
		15 => 'A'
	];
	public $money_table = [
		1 => 100,
		2 => 200,
		3 => 300,
		4 => 500,
		5 => 1000,
		6 => 2000,
		7 => 4000,
		8 => 8000,
		9 => 16000,
		10 => 32000,
		11 => 64000,
		12 => 125000,
		13 => 250000,
		14 => 500000,
		15 => 1000000
	];

	public function evaluate($questionNumber, $userInput) {
		if ($this->correct_answer[$questionNumber] == $userInput) {
			if ($questionNumber != 15) {
				header("Location: Vraag" . ($questionNumber + 1) . ".php");
			} else {
				header("Location: ../won.html");
			}
		} else {
				header("Location: game_over.html");
		}
	}
}
?>
