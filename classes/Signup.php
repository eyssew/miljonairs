<?php

class Signup {
	private	$error = "";
	public function evaluate($data) {
		foreach ($data as $key => $value) {
			if (empty($value)) {
				$error .= $key . " is empty!<br>";
			}
		}

		// Check if the password repetition is the same
		if ($data['password'] == $data['repeated_password']) {
			$error .= "Passwords don't match!<br>";
			return $this->returnResult($error,true);
		}
		
		// Check if the user exists
		$query = "SELECT * FROM users;";
		$DB = new Database();
		$userList = $DB->read($query);
		foreach ($userList as $user) {
			if ($user['username'] === $data['username']) {
				$error .= "Username is already taken!<br>";
				return $this->returnResult($error,true);
			}
		}

		// Add user to the database
		if ($error == "") {
			$this->create_user($data);
			return $this->returnResult("New user created!",false);
		} else {
			return $this->returnResult($error,true);
		}
	}
	
	public function create_user($data) {
		$username = $data['username'];
		$password = $data['password'];

		// Create these:
		$userid = $this->create_userid();
		
		// Insert into database
		$query = "insert into users (userid,username,password) values ('$userid','$username','$password')";
		$DB = new Database();
		$DB->save($query);
	}
	private function create_userid() {
		$length = 15;
		$number = "";
		for ($i=0; $i < $length; $i++) {
			$new_rand = rand(0,9);

			$number = $number . $new_rand;
		}
		return $number;
	}
	public function returnResult($result,$resultIsError) {
		if ($resultIsError) {
			return "<p class='error'>" . $result . "</p>";
		} else {
			return "<p class='succes'>" . $result . "</p>";
		}
	}
}
?>
