<?php

class Login {
	private $error = "";
	public function evaluate($data) {
		foreach ($data as $key => $value) {
			if (empty($value)) {
				$error .= $key . " is empty!<br>";
			}
		}

		if ($error == "") {
			// Check if the user exists
			$query = "SELECT * FROM users;";
			$DB = new Database();
			$userList = $DB->read($query);
			foreach ($userList as $user) {
				if ($user['username'] === $data['username'] && $user['password'] === $data['password']) {
					$_SESSION['userid'] = $data['userid'];
					return $this->returnResult("Logged in!", false);
				}
			}
			$error .= "Username and password don't match!<br>";
			return $this->returnResult($error, true);
		} else {
			return $this->returnResult($error,true);
		}
	}
	public function returnResult($result,$resultIsError) {
		if ($resultIsError) {
			return "<p class='error'>" . $result . "</p>";
		} else {
			$_SESSION['userid'] = $row['userid'];
      header("Location: Vraagpagina/Vraag1.php");
			return "<p class='succes'>" . $result . "</p>";
		}
	}
}
?>
