<?php
include '../base_template.html'; // Include the base template
?>
<html>
	<head>
		<link rel="stylesheet" href="../style.css">
		<title> Miljonairs | Home </title>
	</head>
	<body>

		<div class="selection">
			<h1>Vraag 5:</h1> <br>
			<h2>
				Hoelang duurt de gehele studie geneeskunde?
			</h2>
			<br>
			<form method="post">
			<input type="radio" name="drone" value="A" />
				<label for="A">&nbsp 3 jaar</label>
				<br>
				<input type="radio" name="drone" value="B" />
				<label for="B">&nbsp 4 jaar</label>
				<br>
				<input type="radio" name="drone" value="C" />
				<label for="C">&nbsp 6 jaar</label>
				<br>
				<input type="radio" name="drone" value="D" />
				<label for="D">&nbsp 8 jaar</label>
				<br> <br>
				<br> <br>
				<?php
				include '../classes/checkAnswer.php'; // Include the base template

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$answer = new checkAnswer();
					$answer->evaluate(5, $_POST['drone']);
				}
				?>
				<input class="submit_button" type="submit" value="submit"><br><br>
			</form>
			<input class="stop_button" type="submit" value="Stoppen"><br><br>
		</div>
	</body>
	<script>
    document.getElementsByClassName("stop_button")[0].addEventListener("click", redirectFunction);
        
    function redirectFunction() {
       window.location.href = "../won5.html";
		}
	</script>
</html>
