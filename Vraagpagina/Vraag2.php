<?php
include '../base_template.html'; // Include the base template
?>
<html>
	<head>
		<link rel="stylesheet" href="../style.css">
		<title> Miljonairs | Home </title>
	</head>
	<body>

		<div class="selection">
			<h1>Vraag 2:</h1> <br>
			<h2>
				Hoe heet de eindeloze runner-game waarin de speler
				<br>
				obstakels ontwijkt en munten verzamelt terwijl
				<br>
				hij word achtervolgd door een politieman?
			</h2>
			<br>
			<form method="post">
			<input type="radio" name="drone" value="A" />
				<label for="A">&nbsp Flappy bird</label>
				<br>
				<input type="radio" name="drone" value="B" />
				<label for="B">&nbsp Subway Surfers</label>
				<br>
				<input type="radio" name="drone" value="C" />
				<label for="C">&nbsp Jetpack joyride</label>
				<br>
				<input type="radio" name="drone" value="D" />
				<label for="D">&nbsp Clash Royale</label>
				<br> <br>
				<br> <br>
				<?php
				include '../classes/checkAnswer.php'; // Include the base template

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$answer = new checkAnswer();
					$answer->evaluate(2, $_POST['drone']);
				}
				?>
				<input class="submit_button" type="submit" value="submit"><br><br>
			</form>
			<input class="stop_button" type="submit" value="Stoppen"><br><br>
		</div>
	</body>
	<script>
    document.getElementsByClassName("stop_button")[0].addEventListener("click", redirectFunction);
        
    function redirectFunction() {
       window.location.href = "../won2.html";
		}
	</script>
</html>
