<?php
include '../base_template.html'; // Include the base template
?>
<html>
	<head>
		<link rel="stylesheet" href="../style.css">
		<title> Miljonairs | Home </title>
	</head>
	<body>

		<div class="selection">
			<h1>Vraag 6:</h1> <br>
			<h2>
				Welk sociaal medium is niet van Meta?
			</h2>
			<br>
			<form method="post">
			<input type="radio" name="drone" value="A" />
				<label for="A">&nbsp Facebook</label>
				<br>
				<input type="radio" name="drone" value="B" />
				<label for="B">&nbsp Snapchat</label>
				<br>
				<input type="radio" name="drone" value="C" />
				<label for="C">&nbsp Whatsapp</label>
				<br>
				<input type="radio" name="drone" value="D" />
				<label for="D">&nbsp Instagram</label>
				<br> <br>
				<br> <br>
				<?php
				include '../classes/checkAnswer.php'; // Include the base template

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$answer = new checkAnswer();
					$answer->evaluate(6, $_POST['drone']);
				}
				?>
				<input class="submit_button" type="submit" value="submit"><br><br>
			</form>
			<input class="stop_button" type="submit" value="Stoppen"><br><br>
		</div>
	</body>
	<script>
    document.getElementsByClassName("stop_button")[0].addEventListener("click", redirectFunction);
        
    function redirectFunction() {
       window.location.href = "../won6.html";
		}
	</script>
</html>
