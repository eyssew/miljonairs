<?php
include 'base_template.html';
?>

<!DOCTYPE html>

<html>
  <head>
    <link rel="stylesheet" href="style.css">
    <title> Miljonairs | Home </title>
  </head>
  <body>
    <div class="background">
      <div class="bar">
        <div class="title">
          <p>Miljonairs</p>
        </div>
        <input class="login_button" type="submit" value="login">
        <input class="signup_button" type="submit" value="sign up">
      </div>
      <div class="login_field">
		<h1>Vraag 1:</h1> <br>
		<h2>In welke gebergte ligt mount everest?</h2>
		<br>
		<h3>
			<div class="selection">
				<input type="radio" name="drone" value="A" checked />
				<label for="A">&nbsp Rocky Mountains</label>
				<br>
				<input type="radio" name="drone" value="B" checked />
				<label for="B">&nbsp Alpen</label>
				<br>
				<input type="radio" name="drone" value="C" checked />
				<label for="C">&nbsp Andes</label>
				<br>
				<input type="radio" name="drone" value="D" checked />
				<label for="D">&nbsp Himalaya</label>
			</div>
		</h3>
      </div>
    </div>
  </body>
  <script>

	document.getElementsByClassName("register_button")[0].addEventListener("click", redirectFunctionSignup);
	document.getElementsByClassName("login_button")[0].addEventListener("click", redirectFunctionLogin);
	document.getElementsByClassName("signup_button")[0].addEventListener("click", redirectFunctionSignup);
	document.getElementsByClassName("submit_button")[0].addEventListener("click", redirectFunctionHome);
        
	function redirectFunctionLogin(){
			window.location.href = "../login.php";
	}

	function redirectFunctionHome(){
			window.location.href = "../home.html";
	}

	function redirectFunctionSignup(){
			window.location.href = "../signup.php";
	}
  </script>
</html>