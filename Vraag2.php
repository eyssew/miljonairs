<!DOCTYPE html>

<html>
  <head>
    <link rel="stylesheet" href="../style.css">
    <title> Miljonairs | Home </title>
  </head>
  <body>
    <div class="background">
      <div class="bar">
        <div class="title">
          <p>Miljonairs</p>
        </div>
        <input class="login_button" type="submit" value="login">
        <input class="signup_button" type="submit" value="sign up">
      </div>
      <div class="login_field">
		<h1>Vraag 2:</h1> <br>
		<h2>
			Hoe heet de eindeloze runner-game waarin de speler
			<br>
			obstakels ontwijkt en munten verzamelt terwijl
			<br>
			hij word achtervolgd door een politieman?
		</h2>
		<br>
			<form method="post">
				<input type="radio" name="drone" value="A" />
				<label for="A">&nbsp Flappy bird</label>
				<br>
				<input type="radio" name="drone" value="B" />
				<label for="B">&nbsp Subway Surfers</label>
				<br>
				<input type="radio" name="drone" value="C" />
				<label for="C">&nbsp Jetpack joyride</label>
				<br>
				<input type="radio" name="drone" value="D" />
				<label for="D">&nbsp Clash Royale</label>
				<br> <br>
				<?php
				include '../classes/checkAnswer.php'; // Include the base template

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$answer = new checkAnswer();
					$answer->evaluate(2, $_POST['drone']);
				}
				?>
		<input class="submit_button" type="submit" value="Log in"><br><br>
		</form>
      </div>
    </div>
  </body>
  <script>


	document.getElementsByClassName("login_button")[0].addEventListener("click", redirectFunctionHome);
	document.getElementsByClassName("signup_button")[0].addEventListener("click", redirectFunctionHome);
	document.getElementsByClassName("submit_button")[0].addEventListener("click", reloadFunction);
		
	function redirectFunctionLogin() {
			window.location.href = "../login.php";
	}

	function redirectFunctionHome() {
			window.location.href = "home.html";	
	}

	function redirectFunctionSignup() {
			window.location.href = "../signup.php";
	}

	function reloadFunction() {
		location.reload();
	}

  </script>
</html>