# Miljonairs

A free and open source gambling game to feed your addicition

# Logboek

| Lazhward                    | Date (DD-MM-YY)| Amount of Hours |
| --------------------------- | -------------- | --------------- |
| Lestijd + pauze brainstormen| 1-3-2024       | 2               |
| concept besproken en github | 1-3-2024       | 3               |
| vragen bedacht              | 5-3-2024       | 2               |
| Vragen beantwoord op A4'tje | 7-3-2024       | 1               |
| Quizvragen onderbouwing     | 7-3-2024       | 1               |
| Designtekening              | 7-3-2024       | 0.5             |
| Brainstorm over concept     | 9-3-2024       | 2               |
| hulp programeren            | 15-3-2024      | 2               |
| hulp programeren            | 22-3-2024      | 2               |
| Enquete verslag             | 29-3-2024      | 2               |
| programeren                 | 29-3-2024      | 3               |


| Eysse                       | Date (DD-MM-YY)| Amount of Hours |
| --------------------------- | -------------- | --------------- |
| Lestijd + pauze brainstormen| 1-3-2024       | 2               |
| concept besproken en github | 1-3-2024       | 3               |
| Werk aan code voor database | 6-3-2024       | 3.5             |
| Werk aan login-systeem      | 7-3-2024       | 1.5             |
| ERD gemaakt                 | 7-3-2024       | 1.5             |
| Brainstorm over concept     | 9-3-2024       | 2               |
| Database                    | 11-3-2024      | 2               |
| programeren                 | 15-3-2024      | 2               |
| programeren                 | 17-3-2024      | 2               |
| programeren                 | 22-3-2024      | 2               |
| programeren                 | 23-3-2024      | 3               |
| programeren                 | 29-3-2024      | 5               |


| Olivier                     | Date (DD-MM-YY)| Amount of Hours |
| --------------------------- | -------------- | --------------- |
| Lestijd + pauze brainstormen| 1-3-2024       | 2               |
| concept besproken en github | 1-3-2024       | 1.5             |
| Vragen bedacht van A4'tje   | 7-3-2024       | 1               |
| Quizvragen onderbouwing     | 7-3-2024       | 1               |
| Designtekening              | 7-3-2024       | 0.5             |
| hulp ERD                    | 7-3-2024       | 1.5             |
| Brainstorm over concept     | 9-3-2024       | 2               |
| hulp programeren            | 15-3-2024      | 2               |
| hulp programeren            | 22-3-2024      | 2.5             |
| Enquete                     | 29-3-2024      | 2               |
| programeren                 | 29-3-2024      | 3.5             |