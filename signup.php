<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="style.css">
    <title> Miljonairs | Home </title>
  </head>
  <body>
    <div class="background">
      <div class="bar">
        <div class="title">
          <p>Miljonairs</p>
        </div>
        <input class="login_button" type="submit" value="login">
        <input class="signup_button" type="submit" value="sign up">
      </div>
      <div class="login_field">
        <form method="post" action="">
          <label id="discription" for="username">Register</label><br><br>
          <input name="username" type="text" id="box" name="username" placeholder="Username"><br><br>
          <input name="password" type="password" id="box" name="password" placeholder="Password"><br><br>
          <input name="repeated_password" type="password" id="box" name="repeat password" placeholder="Repeat password"><br><br>
					<br> <br>
					<input class="submit_button" type="submit" value="Register">
					<br> <br>
					<label id="small_discription" for="">Already have an account? </label>
					<input class="register_button" type="submit" value="Log in">
					<?php
					include("classes/Database.php");
					include("classes/Signup.php");

					if ($_SERVER['REQUEST_METHOD'] == 'POST') {
						$signup = new Signup();
						$result = $signup->evaluate($_POST);
						echo "<br> <br>" . $result;
					}
					?>
      </div>
    </div>
  </body>
  <script>
    document.getElementsByClassName("title")[0].addEventListener("click", redirectFunctionHome);
    document.getElementsByClassName("login_button")[0].addEventListener("click", redirectFunctionLogin);
    document.getElementsByClassName("signup_button")[0].addEventListener("click", redirectFunctionSignup);
    document.getElementsByClassName("submit_button")[0].addEventListener("click", redirectFunctionSignup);
        
    function redirectFunctionHome(){
      window.location.href = "home.html";
		}

    function redirectFunctionLogin(){
      window.location.href = "login.php";
		}

    function redirectFunctionSignup(){
      window.location.href = "signup.php";
    }

  </script>
</html>
