<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="style.css">
    <title> Miljonairs | Home </title>
  </head>
  <body>
    <div class="background">
      <div class="bar">
        <div class="title">
          <p>Miljonairs</p>
        </div>
        <input class="login_button" type="submit" value="login">
        <input class="signup_button" type="submit" value="sign up">
      </div>
      <div class="login_field">
        <form method="post" action="">
          <label id="discription" for="username">Login</label><br><br><br>
          <input type="text" id="box" name="username" placeholder="Username"><br><br>
          <input type="password" id="box" name="password" placeholder="Password"><br><br>
          <input class="submit_button" type="submit" value="Log in"><br><br>
        </form>
				<!-- Rounded switch -->
				<label class="switch">
					<input type="checkbox">
					<span class="slider round"></span>
				</label>
				<label id="small_discription" for="">Remember me</label><br><br><br>
				<label id="small_discription" for="">Don't have an account yet? </label>
				<input class="register_button" type="submit" value="Register">
				<?php
				session_start();				

				include("classes/Database.php");
				include("classes/Login.php");

				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$login = new Login();
					$result = $login->evaluate($_POST);
					echo "<br> <br>";
					echo $result;
				} else {
          if (empty($i)) {
            $i = 0;
          }
          $i = $i + 1;
          echo "Not post: " . $i;
        }
				?>
			</div>
    </div>
  </body>
  <script>
    document.getElementsByClassName("title")[0].addEventListener("click", redirectFunctionHome);
    document.getElementsByClassName("signup_button")[0].addEventListener("click", redirectFunctionSignup);
        
    function redirectFunctionHome() {
      window.location.href = "home.php";
    }
    
    function redirectFunctionLogin() {
      window.location.href = "login.php";
    }

    function redirectFunctionSignup() {
      window.location.href = "signup.php";
    }
  </script>
</html>
